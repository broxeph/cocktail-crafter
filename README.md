# Cocktail Crafter

Build and discover cocktail recipes!

## Run the Trap

`docker-compose up --build`

## Stack

- Python 3.7
- Docker
- Postgres 11
- Django 2.2 (currently in beta)
- Bootstrap 4

## To Do

- Static files (bypass Django to serve from nginx)
- GitLab CI/CD
- Create default user for local development
- Deployment
    - Prod docker-compose
    - DigitalOcean?

## Wish List

- SPA
    - DRF
    - JWT
    - Vue 2 (3?)
    - Element UI
- Huey task queue?
    - Simpler than Celery
    - Offload expensive processing if necessary
- Redis?
    - Huey backend/general caching

## Related projects
- https://www.webtender.com/info/
- https://www.cocktaildb.com/
- https://www.cocktailbuilder.com/
